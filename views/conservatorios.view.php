<?php include __DIR__ . '/partials/inicio-doc.part.php'; ?>

<?php include __DIR__ . '/partials/nav.part.php'; ?>

<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>CONSERVATORIOS DE LA COMUNIDAD VALENCIANA</h1>


            <hr class="divider">
            <div class="imagenes_galeria">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">CP</th>
                        <th scope="col">Cursos</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($conservatorios as $conservatorio) : ?>

                        <tr>
                            <th scope="row"><?= $conservatorio->getCodigo() ?></th>

                            <td><?= $conservatorio->getNombre() ?></td>
                            <td><?= $conservatorio->getCP() ?></td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Principal Content Start -->

<?php include __DIR__ . '/partials/fin-doc.part.php'; ?>
