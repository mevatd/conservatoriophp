<?php
require_once 'utils/utils.php';
require_once 'utils/File.php';
require_once 'entity/ImagenGaleria.php';
require_once 'repository/ImagenGaleriaRepository.php';
require_once 'database/Connection.php';
require_once 'core/App.php';


$errores = [];
$descripcion = '';
$mensaje = '';

try {
    $config = require_once 'app/config.php';
    App::bind('config', $config);
    $ImagenGaleriaRepository = new ImagenGaleriaRepository();


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {


        $descripcion = trim(htmlspecialchars($_POST['descripcion']));

        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];

        $imagen = new File('imagen', $tiposAceptados);
        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);


        $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion);
        $ImagenGaleriaRepository->save($imagenGaleria);
        $mensaje = 'se ja giardadp ña o,agem';
        //despies de guiardar resetamo sel parametro de la descriptn
        $descripcion = '';


    }
    $imagenes = $ImagenGaleriaRepository->findAll();
} catch (PDOException2 $exception){
    throw new PDOException2('cuec pdo ' .$exception);
} catch (AppException $e) {
    throw new AppException('app ex ' . $e);
} catch (FileException $e) {
    throw new FileException('shet ' . $e);
}


require 'views/galeria.view.php';