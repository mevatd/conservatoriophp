<?php
require_once 'utils/utils.php';
require_once 'utils/File.php';
require_once 'entity/Conservatorio.php';
require_once 'repository/ConservatorioRepository.php';
require_once 'database/Connection.php';
require_once 'core/App.php';


$errores = [];
$nombre = '';
$descripcion = '';

$mensaje = '';

try {
    $config = require_once 'app/config.php';
    App::bind('config', $config);
    $ConservatorioRepository = new ConservatorioRepository();



    //mostrar asociados
    $conservatorios = $ConservatorioRepository->findAll();


} catch (PDOException2 $exception){
    throw new PDOException2('cuec pdo ' .$exception);
} catch (AppException $e) {
    throw new AppException('app ex ' . $e);
} catch (FileException $e) {
    throw new FileException('shet ' . $e);
}


require 'views/conservatorios.view.php';