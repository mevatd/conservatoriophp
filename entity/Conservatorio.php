<?php
/**
 * Created by PhpStorm.
 * User: lliurex
 * Date: 29/11/2018
 * Time: 10:57
 */


require_once __DIR__ . '/../database/IEntity.php';

//include('C:/xampp/htdocs/php7.local/database/IEntity.php');

class Conservatorio implements iEntity
{


    /**
     * @var int
     */
    private $codigo;
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $cod_prov;

    /**
     * @var string
     */
    private $cod_muni;

    /**
     * @var string
     */
    private $CP;


    public function __construct($nombre = "")
    {
        $this->codigo = null;
        $this->nombre = $nombre;

    }

    /**
     * @return int
     */
    public function getCodigo(): int
    {
        return $this->codigo;
    }

    /**
     * @param int $codigo
     */
    public function setCodigo(int $codigo): void
    {
        $this->codigo = $codigo;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getCodProv(): string
    {
        return $this->cod_prov;
    }

    /**
     * @param string $cod_prov
     */
    public function setCodProv(string $cod_prov): void
    {
        $this->cod_prov = $cod_prov;
    }

    /**
     * @return string
     */
    public function getCodMuni(): string
    {
        return $this->cod_muni;
    }

    /**
     * @param string $cod_mun
     */
    public function setCodMuni(string $cod_muni): void
    {
        $this->cod_muni = $cod_muni;
    }

    /**
     * @return string
     */
    public function getCP(): string
    {

        return $this->getCodProv() . $this->getCodMuni();
    }

    /**
     * @param string $CP
     */
    public function setCP(string $CP): void
    {
        $this->CP = $CP;
    }



    public
    function toArray(): array
    {
        return [
            'nombre' => $this->getNombre(),
            'logo' => $this->getLogo(),
            'cod_prov' => $this->getCodProv(),
            'cod_muni' => $this->getCodMuni()

        ];
    }


}