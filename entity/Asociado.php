<?php
/**
 * Created by PhpStorm.
 * User: lliurex
 * Date: 29/11/2018
 * Time: 10:57
 */


require_once __DIR__ . '/../database/IEntity.php';

//include('C:/xampp/htdocs/php7.local/database/IEntity.php');

class Asociado implements iEntity
{

    const RUTA_LOGOS_ASOCIADOS = 'images/asociados/';

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Asociado constructor.
     * @param string $nombre
     * @param string $logo
     * @param string $descripcion
     */
    public function __construct($nombre = "", $logo = "", $descripcion = "")
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->logo = $logo;
        $this->descripcion = $descripcion;

    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public
    function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Asociado
     */
    public
    function setNombre(string $nombre): Asociado
    {
        $this->nombre = $nombre;
        return $this;
    }


    /**
     * @return string
     */
    public
    function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return Asociado
     */
    public
    function setDescripcion(string $descripcion): Asociado
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo(string $logo): void
    {
        $this->logo = $logo;
    }


    /**
     * @return string
     */
    public
    function __toString()
    {
        return $this->getDescripcion();
    }


    /**
     * @return string
     */
    public
    function getUrlLogo(): string
    {
        return self::RUTA_LOGOS_ASOCIADOS . $this->getLogo();
    }

    public
    function toArray(): array
    {
        return [
            'nombre' => $this->getNombre(),
            'logo' => $this->getLogo(),
            'descripcion' => $this->getDescripcion(),
        ];
    }
}