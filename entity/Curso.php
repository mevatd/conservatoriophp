<?php
/**
 * Created by PhpStorm.
 * User: lliurex
 * Date: 29/11/2018
 * Time: 10:57
 */


require_once __DIR__ . '/../database/IEntity.php';

//include('C:/xampp/htdocs/php7.local/database/IEntity.php');

class Curso implements iEntity
{


    /**
     * @var int
     */
    private $codigo_con;

    /**
     * @var int
     */
    private $codigo_ins;

    /**
     * @var int
     */
    private $matriculados;

    /**
     * @return int
     */
    public function getCodigoCon(): int
    {
        return $this->codigo_con;
    }

    /**
     * @param int $codigo_con
     */
    public function setCodigoCon(int $codigo_con): void
    {
        $this->codigo_con = $codigo_con;
    }

    /**
     * @return int
     */
    public function getCodigoIns(): int
    {
        return $this->codigo_ins;
    }

    /**
     * @param int $codigo_ins
     */
    public function setCodigoIns(int $codigo_ins): void
    {
        $this->codigo_ins = $codigo_ins;
    }

    /**
     * @return int
     */
    public function getMatriculados(): int
    {
        return $this->matriculados;
    }

    /**
     * @param int $matriculados
     */
    public function setMatriculados(int $matriculados): void
    {
        $this->matriculados = $matriculados;
    }




    public
    function toArray(): array
    {
        return [
            'codigo_con' => $this->getCodigoCon(),
            'codigo_ins' => $this->getCodigoIns(),
            'matriculados' => $this->getMatriculados()
        ];
    }

}