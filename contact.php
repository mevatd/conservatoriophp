<?php
require 'utils/utils.php';
require_once 'database/Connection.php';
require_once 'core/App.php';
$errores = [];
$mensaje = '';


$nombre = '';
$apellidos = '';
$email = '';
$asunto = '';
$texto = '';

try {
    $config = require_once 'app/config.php';
    App::bind('config', $config);
    //recuperamos la conexion
    $connection = Connection::getConnection();

    $sql = "INSERT INTO mensajes(nombre, apellidos, asunto, email, texto)
VALUES (:nombre, :apellidos, :asunto, :email, :texto)";

    $pdoStatement = $connection->prepare($sql);
    $parameters = [':nombre' => $nombre, ':apellidos' => $apellidos, ':asunto' => $email, ':email' => $email, ':texto' => $asunto];

    if ($pdoStatement->execute($parameters) === false) {
        $errores[] ="no se ha podido guardar la mensaje en la bda";
    }else{
        $mensaje = 'se ha guardado el mensaje ne la bda';
        $descripcion='';
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $nombre = trim(htmlspecialchars($_POST['nombre']));
        $apellidos = trim(htmlspecialchars($_POST['apellidos']));
        $email = trim(htmlspecialchars($_POST['email']));
        $asunto = trim(htmlspecialchars($_POST['asunto']));
        $texto = trim(htmlspecialchars($_POST['texto']));
        if (empty($nombre))
            $errores[] = "El nombre no puede estar vacio";
        if (empty($email))
            $errores[] = "El email no puede estar vacio";
        else {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
                $errores[] = "El email no es valido";
        }
        if (empty($asunto))
            $errores[] = "El asunto no puede estar vacio";
        if (empty ($errores))
            $mensaje = "Los datos del formulario son correctos";

    }
} catch (Exception $exception) {

}


require 'views/contact.view.php';
