<?php
/**
 * Created by PhpStorm.
 * User: 4710HQ
 * Date: 12/12/2018
 * Time: 2:07
 */

interface IEntity
{
    public function toArray(): array;

}