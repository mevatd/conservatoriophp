<?php
return [
    'database' => ['name' => 'conservatorio_db',
        'username' => 'root',
        'password' => '',
        'connection' => 'mysql:host=127.0.0.2',
        'options' => [

            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ]
];